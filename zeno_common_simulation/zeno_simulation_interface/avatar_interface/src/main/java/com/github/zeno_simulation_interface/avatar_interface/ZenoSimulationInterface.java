/*
 * Copyright (C) 2014 shehzad.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.github.zeno_simulation_interface.avatar_interface;

//ros imports
import org.apache.commons.logging.Log;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeMain;
import org.ros.node.parameter.ParameterTree;
import org.ros.node.topic.Subscriber;
import org.ros.node.topic.Publisher;

//import std_msgs.*;

//zeno imports
import org.robokind.client.basic.UserSettings;
import org.robokind.api.motion.messaging.RemoteRobot;
import org.robokind.api.motion.messaging.RemoteRobotClient;
import org.robokind.api.motion.Robot.RobotPositionMap;
import org.robokind.client.basic.Robokind;
import org.robokind.api.motion.Joint;
import org.robokind.api.motion.Robot.JointId;
import org.robokind.client.basic.RobotJoints;
import org.robokind.api.common.position.NormalizedDouble;

import trajectory_msgs.JointTrajectoryPoint;

import java.util.Collections;
import java.util.List;
//vector
import java.util.Vector;
import java.lang.String;
import java.lang.reflect.Field;

/**
 * A simple {@link Subscriber} {@link NodeMain}.
 */


public class ZenoSimulationInterface extends AbstractNodeMain 
{	
		private Subscriber<trajectory_msgs.JointTrajectory> trajectory_command_body_subscriber, trajectory_command_head_subscriber;
		private Publisher<std_msgs.String> event_out_pub;
		private Subscriber<std_msgs.String> event_in_subscriber;
		private std_msgs.String event_out_msg;
		private ParameterTree params;

		private double joint_limit_tolerance;
		private double execution_time_scale;
		private RemoteRobot myRobot;
		private Log log;
		
		String lock;

		@Override
		public GraphName getDefaultNodeName() 
		{
				return GraphName.of("zeno_simulation_interface_node");
		}
		
		@Override
		public void onStart(ConnectedNode connectedNode) 
		{				
				log = connectedNode.getLog();
				
				lock = "unlocked";

				event_out_pub = connectedNode.newPublisher(
					                          "/ZenoSkeletonController/event_out", std_msgs.String._TYPE);

				trajectory_command_body_subscriber = connectedNode.newSubscriber
																	("/body_controller/trajectory_command_sim", 
																	 trajectory_msgs.JointTrajectory._TYPE);
				
				trajectory_command_head_subscriber = connectedNode.newSubscriber
																	("/head_controller/trajectory_command_sim", 
																	 trajectory_msgs.JointTrajectory._TYPE);
				
				event_in_subscriber = connectedNode.newSubscriber(
                        										"/ZenoSkeletonController/event_in", std_msgs.String._TYPE);


				event_out_msg = event_out_pub.newMessage();
				
				params = connectedNode.getParameterTree();
				
				boolean simulation = params.getBoolean("~simulation", true);
				
				joint_limit_tolerance = params.getDouble("~joint_limit_tolerance", 0.01);
				
				execution_time_scale = params.getDouble("~execution_time_scale", 0.2);

				java.lang.String ip = "zeno";
			    if(!simulation) {
			    	UserSettings.setSensorAddress(ip);
			        UserSettings.setAccelerometerAddress(ip);
			        UserSettings.setGyroscopeAddress(ip);
			        UserSettings.setCompassAddress(ip);
			        UserSettings.setRobotAddress(ip);
			        UserSettings.setSpeechAddress(ip);
			        UserSettings.setRobotId("myRobot");
			        UserSettings.setCameraAddress(ip);
			        UserSettings.setAnimationAddress(ip);
			        UserSettings.setCameraId("0");
				}
		        
				myRobot = Robokind.connectRobot();
				if (!myRobot.isConnected())
					log.error("Unable to connect to robot");
				
				event_in_subscriber.addMessageListener(new MessageListener<std_msgs.String>() 
				{
					@Override 
					public void onNewMessage(std_msgs.String message)
					{
						String msg = message.getData();
						if (msg.equals("e_stop")) {
							lock = "locked";
							log.info("Zeno simulation controller stopped.");
							event_out_msg.setData("e_stopped");

							event_out_pub.publish(event_out_msg);
						} else if (msg.equals("e_start")) {
							lock = "unlocked";
							log.info("Zeno simulation controller started.");
							event_out_msg.setData("e_started");

							event_out_pub.publish(event_out_msg);
						}
							
					}
				});

				trajectory_command_body_subscriber.addMessageListener(new MessageListener<trajectory_msgs.JointTrajectory>() 
				{
					@Override 
					public void onNewMessage(trajectory_msgs.JointTrajectory message)
					{
						boolean success = false;
						if (lock.equals("unlocked")) {
							success = executeTrajectoryCommand(message);
						}
						
						event_out_msg.setData("e_success");
						if (!success)
							event_out_msg.setData("e_failed");
							
						event_out_pub.publish(event_out_msg);	
					}
				});
				
				trajectory_command_head_subscriber.addMessageListener(new MessageListener<trajectory_msgs.JointTrajectory>() 
				{
					@Override 
					public void onNewMessage(trajectory_msgs.JointTrajectory message)
					{
						boolean success = false;
						if (lock.equals("unlocked")) {
							success = executeTrajectoryCommand(message);
						}
						
						event_out_msg.setData("e_success");
						if (!success)
							event_out_msg.setData("e_failed");
							
						event_out_pub.publish(event_out_msg);
					} 
				});
		}
		
		@Override
		public void onShutdown(Node node) {
			log.info("Shutdown " + getDefaultNodeName());	
		}
		
		@Override
		public void onShutdownComplete(Node node) {
		}

	    private boolean executeTrajectoryCommand(trajectory_msgs.JointTrajectory message)
		{
			List<String> joint_names = message.getJointNames();
			List<JointTrajectoryPoint> trajectory = message.getPoints();

	    	if (!myRobot.isConnected()) {
				log.error("Robot is disconnected");
				return false;
	    	}
		  
			try {	
					for (int point_index=0; point_index < trajectory.size();point_index++) {
						
						RobotPositionMap currentPositions = myRobot.getDefaultPositions();
						RobotPositionMap goalPositions = currentPositions;
						currentPositions.clear();
						goalPositions.clear();
						
						currentPositions = getCurrentJointPositions();
						
						if(currentPositions == null) return false;
						
						
						double joint_positions[] = trajectory.get(point_index).getPositions();
						Vector< Double > execution_time = new Vector< Double >();
						
						for (int i = 0; i < joint_names.size(); i++) {
							String joint_name = joint_names.get(i).toUpperCase();
					        
							Field f1 = RobotJoints.class.getField(joint_name);
					        int joint_id = (Integer) f1.get(f1);
					        
					        double desired_value = joint_positions[i];
					
					        if(desired_value > 0.0 &&  desired_value < 1.0) {
					        	JointId jointid = new JointId(myRobot.getRobotId(), new Joint.Id(joint_id));
					        	
					        	//compute angular distance
					        	double current_value = currentPositions.get(jointid).getValue();
					        	
					        	double angular_displacement = Math.abs(Math.abs(current_value) - Math.abs(desired_value));
					        	if (angular_displacement > joint_limit_tolerance) {
					        	//log.info("current_value: "+ current_value + " joint_value: " + desired_value);
					        		execution_time.add(new Double(computeExecutionTime(angular_displacement)));
					        	//add goal positions
					        		goalPositions.put(jointid, new NormalizedDouble(desired_value));
					        	}
					        } else {
					        	log.warn(joint_name + ": value is out of bound.");
					        }
						}
						if(!goalPositions.isEmpty())
							myRobot.move(goalPositions, findTrajectoryExecutionTime(execution_time));
					}
			}  catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			
			return true;
		}
	    
		private double computeExecutionTime(double angular_displacement)
		{
			double delta_time = (1+Math.exp(-angular_displacement))*(1000*execution_time_scale);
			return delta_time;
		}
		
		private long findTrajectoryExecutionTime(Vector< Double > angular_distances )
		{
			double max_time = Collections.max(angular_distances).doubleValue();
			return (long)(max_time);
		}
		
		private RobotPositionMap getCurrentJointPositions()
		{
			RobotPositionMap currentPositions = null;
			try {
					Field field = myRobot.getClass().getDeclaredField("myRobotClient");
					field.setAccessible(true);
					RemoteRobotClient client = (RemoteRobotClient) field.get(myRobot);
					currentPositions = client.requestCurrentPositions();
			}  catch (Exception e) {
		    	e.printStackTrace();
		    }
			
			return currentPositions;
		}
}
