# Android ROS-bluetooth api interface for Zeno

# Running:
  
  Manually:
    
    cd /zeno_java_control_interface/RemoteBlueToothServer/build/install/RemoteBlueToothServer/bin
    ./RemoteBlueToothServer com.github.zeno_java_control_interface.RemoteBlueToothServer.RemoteBluetoothServer

  or Using roslaunch
    
    roscd zeno_java_control_interface/RemoteBlueToothServer/launch/
    roslaunch remote_bluetooth_server.launch