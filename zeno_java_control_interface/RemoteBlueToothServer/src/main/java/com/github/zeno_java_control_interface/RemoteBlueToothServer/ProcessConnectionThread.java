//package com.luugiathuy.apps.remotebluetooth;
package com.github.zeno_java_control_interface.RemoteBlueToothServer;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.InputStream;

import org.ros.node.topic.Publisher;
import org.ros.node.ConnectedNode;

import javax.microedition.io.StreamConnection;

public class ProcessConnectionThread implements Runnable{

	private StreamConnection mConnection;
	private ConnectedNode mconnectedNode;
	private Publisher<std_msgs.String> event_out_pub;
	private std_msgs.String event_out_msg;

	
	// Constant that indicate command from devices
	private static final int EXIT_CMD = -1;
	private static final int KEY_RIGHT = 1;
	private static final int KEY_LEFT = 2;
	private static final int KEY_SkEnable = 10;
	private static final int KEY_SkDisable = 11;
	private static final int KEY_FaceEnable = 12;
	private static final int KEY_FaceDisable = 13;
	
	public ProcessConnectionThread(StreamConnection connection, ConnectedNode connectedNode)
	{
		mConnection = connection;
		mconnectedNode = connectedNode;
		System.out.println("ProcessConnectionThread.");
	}
	
	@Override
	public void run() {
		try {
			System.out.println("run.");
			event_out_pub = mconnectedNode.newPublisher(
					                          "/remote_bluetooth_server/event_out", std_msgs.String._TYPE);
			// prepare to receive data
			InputStream inputStream = mConnection.openInputStream();
			event_out_msg = event_out_pub.newMessage();
	        
			System.out.println("waiting for input");
	        
	        while (true) {
	        	int command = inputStream.read();
	        	
	        	if (command == EXIT_CMD)
	        	{	
	        		System.out.println("finish process");
	        		break;
	        	}
	        	
	        	processCommand(command);
        	}
        } catch (Exception e) {
    		e.printStackTrace();
    	}
	}
	
	/**
	 * Process the command from client
	 * @param command the command code
	 */
	private void processCommand(int command) {
		try {
			Robot robot = new Robot();
			switch (command) {
	    	case KEY_RIGHT:
	    		robot.keyPress(KeyEvent.VK_RIGHT);
	    		System.out.println("Right");
	    		// release the key after it is pressed. Otherwise the event just keeps getting trigged	    		
	    		robot.keyRelease(KeyEvent.VK_RIGHT);
	    		break;
	    	case KEY_LEFT:
	    		robot.keyPress(KeyEvent.VK_LEFT);
	    		System.out.println("Left");
	    		// release the key after it is pressed. Otherwise the event just keeps getting trigged	    		
	    		robot.keyRelease(KeyEvent.VK_LEFT);
	    		break;
	    	case KEY_SkEnable:
	    		System.out.println("Enable Skeleton Tracking");
	    		event_out_msg.setData("e_enable_skeleton_tracking");
	    		event_out_pub.publish(event_out_msg);
	    		break;
	    	case KEY_SkDisable:
	    		System.out.println("Disable Skeleton Tracking");
	    		event_out_msg.setData("e_disable_skeleton_tracking");
	    		event_out_pub.publish(event_out_msg);
	    		break;
	    	case KEY_FaceEnable:
	    		System.out.println("Enable Face Tracking");
	    		event_out_msg.setData("e_enable_face_tracking");
	    		event_out_pub.publish(event_out_msg);
	    		break;
	    	case KEY_FaceDisable:
	    		System.out.println("Disable Face Tracking");
	    		event_out_msg.setData("e_disable_face_tracking");
	    		event_out_pub.publish(event_out_msg);
	    		break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
