//package com.luugiathuy.apps.remotebluetooth;

package com.github.zeno_java_control_interface.RemoteBlueToothServer;
/*
public class RemoteBluetoothServer{
	
	public static void main(String[] args) {
		Thread waitThread = new Thread(new WaitThread());
		waitThread.start();
	}
}
*/
import org.ros.concurrent.CancellableLoop;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.NodeMain;
import org.ros.node.topic.Publisher;

public class RemoteBluetoothServer extends AbstractNodeMain{

    @Override
    public GraphName getDefaultNodeName() {
    return GraphName.of("remote_bluetooth_server");
    }
    
    @Override
    public void onStart(final ConnectedNode connectedNode) {
        System.out.println("connectedNode.");
        Thread waitThread = new Thread(new WaitThread(connectedNode));
        System.out.println("thread running.");
        waitThread.start();
    }
}
